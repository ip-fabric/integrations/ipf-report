# IP Fabric PDF Report Generator

## IP Fabric

IP Fabric is a vendor-neutral network assurance platform that automates the 
holistic discovery, verification, visualization, and documentation of 
large-scale enterprise networks, reducing the associated costs and required 
resources whilst improving security and efficiency.

It supports your engineering and operations teams, underpinning migration and 
transformation projects. IP Fabric will revolutionize how you approach network 
visibility and assurance, security assurance, automation, multi-cloud 
networking, and trouble resolution.

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Installation

```console
$ git clone https://gitlab.com/ip-fabric/integrations/ipf-report.git
$ cd ipf-report
$ apt install weasyprint
$ pip install poetry
$ poetry install
```

Ensure you export your IP Fabric credentials as environment variables:

```console
$ export IPF_URL=https://<ip-fabric-url>
$ export IPF_TOKEN=<ip-fabric-token>
```

or change the `.env` file with your credentials.

## Usage
There are a few example reports included in the `reports` directory. Which can be used as a starting point for creating your own reports.

The `data.yml` file is an example of the data that can be used in the template. The data can also be used to change the branding of the report. So you can add custom logos, colors, etc.

```console
Args:
    html: A file object containing the HTML template.
    css: A file object containing the CSS template.
    data: A file object containing the data to be used for rendering the templates.
    output: The output file path for the generated PDF report.
    site: The name of the site to generate the report for.

Returns:
    None

$ generate [OPTIONS]
```

**Options**:

* `--html FILENAME`: [required]
* `--css FILENAME`: [required]
* `--data FILENAME`: [required]
* `--output TEXT`: [default: output.pdf]
* `--site TEXT`
* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

## Examples

```console
ipf-report --html reports/data_report.html --css reports/style.css --data data.yml
```

## Creating a template
The HTML file can be any HTML file with Jinja2 templating.

There are some variables that are available for use in the template:

* `site`: The name of the site.
any other variable in the data file.

There are some custom Jinja2 functions that are available for use in the template:
* `client`: Returns the IP Fabric client. e.g. `{% set ipf = client() %}}` client accepts any arguments that can be passed to the IPFClient class.
* `site_topology`: Returns the an SVG site topology. It accepts the client and a site name. e.g. `{% set topology = site_topology(ipf, site) %}`
* `delta`: Returns the delta between two values. It accepts two numbers. e.g. `{% set delta = delta(data1, data2) %}` and will return the difference between the two values. e.g. `+10` or `-10`.
* `graphs`: Returns a graph client. This will give you access to `graphs.bar_chart`, `graphs.stacked_bar_chart` and `graphs.pie_chart`.
  * `bar_chart`: Returns a bar chart. It accepts data in the form of `list[dict]`, `x_col` is the key in the dict that will be used for the x-axis and `y_cols` is a list of keys in the dict that will be used for the y-axis. `x_label` and `y_label` are the labels for the x and y axis. `title` is the title of the graph. e.g. Lets say we want to get a graph of the number of devices per vendor.
    The Jinja2 code would look like this:
    ```jinja2
    {% set graph = graphs.bar_chart(ipf.inventory.vendors.all(), x_col='vendor', y_cols=['deviceCount'], x_label='Vendor', y_label='Count', title='Number of devices per vendor') %}
    {{ graph }}
    ```
    * `stacked_bar_chart`: Returns a stacked bar chart. It accepts data in the form of `list[dict]`, `x_col` is the key in the dict that will be used for the x-axis and `y_cols` is a list of keys in the dict that will be used for the y-axis. `x_label` and `y_label` are the labels for the x and y axis. `title` is the title of the graph. e.g. Lets say we want to get a graph of the number of devices per vendor and the number of models per vendor.

    The Jinja2 code would look like this:
    ```jinja2
    {% set graph = graphs.stacked_bar_chart(ipf.inventory.vendors.all(), x_col='vendor', y_cols=['deviceCount', 'modelCount'], x_label='Vendor', y_label='Count', title='Number of devices and models per vendor') %}
    {{ graph }}
    ```
    * `pie_chart`: Returns a pie chart. It accepts data in the form of `list[dict]`, `x_col` is the key in the dict that will be used for the segments and `y_col` will determine the size of the segments. `title` is the title of the graph. `legend_labels` is a boolean that will determine if the legend should be displayed. `show_pct` is a boolean that will determine if the percentage should be displayed on the graph instead of the raw number. e.g. Lets say we want to get a graph of the number of devices per vendor.

    The Jinja2 code would look like this:
    ```jinja2
    {% set graph = graphs.pie_chart(ipf.inventory.vendors.all(), x_col='vendor', y_col='deviceCount', title='Number of devices per vendor', legend_labels=True, show_pct=True) %}
    {{ graph }}
    ```
    * `count_values`: Returns counts for columns in a table. It accepts data in the form of `list[dict]`, `columns` is a list of keys in the dict that will be used for the counts. e.g. Lets say we want to get the count each interface that is `up` and `down`.
    The Jinja2 code would look like this:
    ```jinja2
    {% set counts = count_values(ipf.inventory.interfaces.all(), columns=['l1']) %}
    {{ counts }}
    ```

    The result would be a dict with the counts for each column. e.g. `{'l1': {'up': 10, 'down': 5}}`

    This can then be used to create a graph:

    ```jinja2
      {% set graph_data = graphs.bar_chart(graphs.count_values(ipf.inventory.interfaces.all(), ['l1', 'l2']), 'l1', ['up', 'down'], x_label="Layer 1/2 Status", y_label="count", title="Interface Status") %}
      {{ graph_data }}
    ```
