import matplotlib.pyplot as plt
import numpy as np
from io import BytesIO


class Graphs:
    def __init__(self):
        pass

    def bar_chart(
        self, data, x_col, y_cols=None, x_label=None, y_label=None, title=None
    ):
        """
        Creates a bar chart from a list of dictionaries or a dictionary and returns the SVG representation.

        Args:
            data (list or dict): Data to create the bar chart. If list, it should be a list of dictionaries representing the data.
                                If dict, it should be a dictionary where each key represents a category and each value represents its count.
            x_col (str): Name of the column to use for the x-axis labels if data is a list of dictionaries.
            y_cols (list): List of column names to include in the chart (for grouped bars) if data is a list of dictionaries. Default is None.
            x_label (str): Label for the x-axis.
            y_label (str): Label for the y-axis.
            title (str): Title of the chart.

        Returns:
            str: Base64 encoded SVG data of the bar chart.
        """
        # Check if data is empty
        if not data:
            raise ValueError("Data list is empty")

        # If data is a dictionary, extract x_col and y_cols from the keys and values
        if isinstance(data, dict):
            if not all(isinstance(val, dict) for val in data.values()):
                raise ValueError(
                    "Invalid data format. If data is a dictionary, its values must be dictionaries."
                )
            categories = list(data.keys())
            if not y_cols:
                y_cols = list(data[categories[0]].keys())
            y_axis = np.array(
                [[data[key].get(col, 0) for col in y_cols] for key in categories]
            )
            x = np.arange(len(categories))
        # If data is a list of dictionaries, extract x_col and y_cols from the list
        elif isinstance(data, list) and isinstance(data[0], dict):
            categories = [item.get(x_col) for item in data]
            if not y_cols:
                y_cols = list(data[0].keys())
            y_axis = np.array([[item.get(col, 0) for col in y_cols] for item in data])
            if all(isinstance(x, (int, float)) for x in categories):
                x = categories
            else:
                x = np.arange(len(categories))
        else:
            raise ValueError(
                "Invalid data format. Data must be a list of dictionaries or a dictionary."
            )

        # Create the bar chart with grouped positioning
        plt.figure(figsize=(10, 6))
        fig, ax = plt.subplots()
        width = 0.35  # Adjust bar width as desired
        n_attributes = (
            len(y_cols) if y_cols else 0
        )  # Number of attributes (y-axis columns)
        index = np.arange(len(x))  # Base x-axis positions

        for i, attribute in enumerate(y_cols):
            offset = (i - n_attributes / 2) * (
                width / n_attributes
            )  # Group bars centered around the index
            rects = ax.bar(
                index + offset, y_axis[:, i], width / n_attributes, label=attribute
            )
            ax.bar_label(rects, padding=3)

        # Customize the chart
        ax.set_xlabel(x_label if x_label else x_col)
        ax.set_ylabel(y_label if y_label else ", ".join(y_cols))
        ax.set_title(title if title else "Attributes by " + x_col)
        ax.set_xticks(index)
        ax.set_xticklabels(
            categories
            if not all(isinstance(x, (int, float)) for x in categories)
            else x,
            rotation=45,
            ha="right",
        )
        ax.legend(loc="upper left")

        plt.tight_layout()

        return self._save_svg(fig)

    def pie_chart(
        self, data, x_col, y_col=None, title=None, legend_labels=None, show_pct=False
    ):
        """
        Creates a pie chart from data and returns the SVG representation.

        Args:
            data (list or dict): Data to create the pie chart. If list, it should be a list of dictionaries representing the data.
                                If dict, it should be a dictionary where each key represents a category and each value represents its count.
            x_col (str): Name of the column to use for the pie slices if data is a list of dictionaries.
            y_col (str): Name of the column to use for the pie slice sizes if data is a list of dictionaries. Optional if data is a dict.
            title (str): Title of the chart.
            legend_labels (list): List of labels for the legend.
            show_pct (bool): If True, display percentages on the pie slices; otherwise, display numbers.

        Returns:
            str: Base64 encoded SVG data of the pie chart.
        """
        # Check if data is empty
        if not data:
            raise ValueError("Data is empty")

        # If data is a dictionary, extract x_col and y_col from the keys and values
        if isinstance(data, dict):
            if not isinstance(data.get(x_col), dict):
                raise ValueError(
                    "Invalid data format. If data is a dictionary, its values must be dictionaries."
                )
            labels = list(data.get(x_col).keys())
            sizes = list(data.get(x_col).values())
        # If data is a list of dictionaries, extract x_col and y_col from the list
        elif isinstance(data, list) and isinstance(data[0], dict):
            labels = [item.get(x_col) for item in data]
            sizes = [item.get(y_col) for item in data]
        else:
            raise ValueError(
                "Invalid data format. Data must be a list of dictionaries or a dictionary."
            )

        # Create the pie chart
        plt.figure(figsize=(8, 8))
        fig, ax = plt.subplots()
        wedges, texts, autotexts = ax.pie(
            sizes,
            labels=legend_labels if legend_labels else labels,
            autopct=lambda pct: self._pie_autopct(pct, sizes, show_pct),
            startangle=90,
        )

        # Customize the chart
        ax.set_title(title if title else "Pie Chart")
        ax.legend(
            wedges,
            labels,
            title=x_col,
            loc="center left",
            bbox_to_anchor=(1, 0, 0.5, 1),
        )

        plt.tight_layout()

        return self._save_svg(fig)

    def stacked_bar_chart(
        self, data, x_col, y_cols, x_label=None, y_label=None, title=None
    ):
        """
        Creates a stacked bar chart showing the counts of different attributes for each category.

        Args:
            data (list): List of dictionaries representing the data.
            x_col (str): Name of the column to use for the x-axis labels.
            y_cols (list): List of column names to include in the chart (for stacked bars).
            x_label (str): Label for the x-axis.
            y_label (str): Label for the y-axis.
            title (str): Title of the chart.

        Returns:
            str: Base64 encoded SVG data of the stacked bar chart.
        """
        # Check if data is empty
        if not data:
            raise ValueError("Data list is empty")

        # Extract data for each column
        categories = [row.get(x_col) for row in data]
        y_axis = [[row.get(col, 0) for col in y_cols] for row in data]

        # Convert y-axis elements to numeric if necessary
        try:
            y_axis = np.array([[float(val) for val in row] for row in y_axis])
        except ValueError as e:
            raise ValueError(f"Error converting y-axis data to float: {e}")

        # Create the stacked bar chart
        plt.figure(figsize=(12, 6))
        plt.bar(categories, y_axis[:, 0], label=y_cols[0])
        bottom = y_axis[:, 0]
        for i in range(1, len(y_cols)):
            plt.bar(categories, y_axis[:, i], bottom=bottom, label=y_cols[i])
            bottom += y_axis[:, i]

        # Customize the chart
        plt.xlabel(x_label if x_label else x_col)
        plt.ylabel(y_label if y_label else "Counts")
        plt.title(title if title else "Stacked Bar Chart")
        plt.legend()
        plt.tight_layout()

        return self._save_svg(
            plt.gcf()
        )  # Pass the Figure object (plt.gcf()) instead of plt

    def _pie_autopct(self, pct, all_values, show_pct):
        """
        Custom function to format pie chart labels with either percentage or numeric value or both.

        Args:
            pct (float): Percentage value of the slice.
            all_values (list): List of all slice sizes.
            show_pct (bool): If True, display percentages; otherwise, display numbers.

        Returns:
            str: Formatted string with percentage and/or numeric value.
        """
        absolute = int(pct / 100.0 * np.sum(all_values))
        if show_pct:
            return f"{pct:.1f}%\n({absolute:d})"
        else:
            return f"{absolute:d}"

    def count_values(self, data, columns):
        """
        Count the occurrences of unique combinations of values in specified columns of the data.

        Args:
            data (list of dict): List of dictionaries representing the data.
            columns (list of str): Names of the columns to count values.

        Returns:
            dict: A dictionary containing the counts of unique combinations of values in the specified columns.
        """
        counts = {column: {} for column in columns}
        for row in data:
            for column in columns:
                value = row.get(column)
                counts[column][value] = counts[column].get(value, 0) + 1

        return counts

    def _save_svg(self, fig):
        """
        Saves the given Matplotlib figure as an SVG and returns the Base64 encoded string.

        Args:
            fig (matplotlib.figure.Figure): Matplotlib figure to be saved.

        Returns:
            str: Base64 encoded SVG data of the figure.
        """
        buffer = BytesIO()
        fig.savefig(buffer, format="svg")
        plt.close(fig)  # Close the figure to clear memory

        # svg_data = base64.b64encode(buffer.getvalue()).decode("utf-8")
        svg_data = buffer.getvalue().decode("utf-8")
        buffer.close()

        # return f"data:image/svg+xml;base64,{svg_data}"
        return svg_data
