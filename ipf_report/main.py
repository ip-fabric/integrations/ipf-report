from jinja2 import Environment
from ipfabric import IPFClient
from ipfabric.diagrams import Network, NetworkSettings
import base64
import ssl
import typer
from typing_extensions import Annotated
import yaml
from weasyprint import HTML, CSS
from .graphs import Graphs
from rich.progress import Progress, SpinnerColumn, TimeElapsedColumn

graphs = Graphs()

ssl._create_default_https_context = ssl._create_unverified_context

app = typer.Typer()


def client(**kwargs):
    """
    Create an IPFClient instance with the provided keyword arguments.

    Args:
        **kwargs: Keyword arguments to be passed to the IPFClient constructor.

    Returns:
        IPFClient: An instance of IPFClient.
    """
    client = IPFClient(**kwargs)
    return client


def delta(snapshot_1, snapshot_2):
    """
    Calculate the difference between two snapshots.

    Args:
        snapshot_1: The first snapshot.
        snapshot_2: The second snapshot.

    Returns:
        str: The difference between the two snapshots.
    """
    delta = snapshot_1 - snapshot_2
    if delta > 0:
        return f"+{delta}"
    elif delta < 0:
        return f"{delta}"
    else:
        return delta


def create_dict(data, key):
    """
    Create a dictionary from a list of dictionaries using a specific key.

    Args:
        data: A list of dictionaries.
        key: The key to use for creating the dictionary.

    Returns:
        dict: A dictionary with the specified key.
    """
    return {item[key]: item for item in data}


def site_topology(client, site_name):
    """
    Get the site topology and network settings for a given site.

    Args:
        client: An instance of IPFClient.
        site_name: The name of the site.

    Returns:
        dict: A dictionary containing the JSON and SVG data for the site topology.
    """
    # Get the site topology
    net = Network(sites=[site_name], all_network=False)
    # Get the network settings
    settings = NetworkSettings()
    # Set the network settings
    settings.ungroup_group("Layer 3")
    # Generate the JSON data
    json_data = client.diagram.json(net, graph_settings=settings)
    # Generate the SVG data
    svg_data = client.diagram.svg(net, graph_settings=settings)
    # Encode the SVG data to base64
    encoded_svg_data = base64.b64encode(svg_data).decode("utf-8")

    # Return the JSON and SVG data
    return {
        "json_data": json_data,
        "svg_data": f"data:image/svg+xml;base64,{encoded_svg_data}",
    }


def render_template(file, data):
    """
    Render a template using Jinja2.

    Args:
        file: A file object containing the template content.
        data: A dictionary containing the data to be used for rendering the template.

    Returns:
        str: The rendered output.
    """
    # Read the template content from the file object
    template_content = file.read()

    # Set up the Jinja2 environment and add the custom filter
    env = Environment()

    # Load the template from the string content
    template = env.from_string(template_content)

    # Define context for rendering the template
    context = {
        "client": client,
        "site_topology": site_topology,
        "delta": delta,
        "graphs": graphs,
        "create_dict": create_dict,
    }
    context.update(data)

    # Render the template
    rendered_output = template.render(context)

    return rendered_output


@app.command()
def generate(
    html: Annotated[typer.FileText, typer.Option()],
    css: Annotated[typer.FileText, typer.Option()],
    data: Annotated[typer.FileText, typer.Option()] = None,
    output: Annotated[str, typer.Option()] = "output.pdf",
    site: Annotated[str, typer.Option()] = None,
):
    """
    Generate a PDF report using HTML and CSS templates.

    Args:
        html: A file object containing the HTML template.
        css: A file object containing the CSS template.
        data: A file object containing the data to be used for rendering the templates.
        output: The output file path for the generated PDF report.
        site: The name of the site to generate the report for.

    Returns:
        None
    """

    with Progress(
        SpinnerColumn(),
        *Progress.get_default_columns(),
        TimeElapsedColumn(),
        transient=True,
    ) as progress:
        task1 = progress.add_task("[red]Generating...", total=100)

        if data:
            progress.update(task1, description="Loading Data...", advance=10)
            data = yaml.safe_load(data.read())
        else: # If no data file is provided, use an empty dictionary
            data = {}

        if site:
            data["site"] = site

        progress.update(task1, description="Rendering HTML...", advance=50)
        html_output = render_template(html, data)

        progress.update(task1, description="Rendering CSS...", advance=70)
        css_output = render_template(css, data)

        progress.update(task1, description="Generating PDF...", advance=90)
        HTML(string=html_output).write_pdf(output, stylesheets=[CSS(string=css_output)])

        # Print the output file path
        progress.update(task1, description="Report generated...", advance=100)
        progress.console.print(f"Report generated: {output}")
